\ **********************************************************************
\
\ max7219defs.fs is part of the ff-max2719 project
\ ff-max2719 is a project to control the max2719 chip with FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ The purpose of this file is to provide constants specific to
\ interacting with the max7219.
\
\ Assumes the LOAD pin is PD4 (Arduino DP4). LOAD pin may be labelled
\ CS pin but max7219 is not fully SPI compatible, so LOAD is correct
\ term per the data sheet.
\
\ public words:
\   LOAD_DDR LOAD_PORTR LOAD_BT NOOP_MSK D0_MSK D1_MSK D2_MSK D3_MSK
\   D4_MSK D5_MSK D6_MSK D7_MSK DEC_MODE_MSK INTEN_MSK SCAN_LIM_MSK
\   SHUTDN_MSK DISP_TST_MSK BCD_0 BCD_1 BCD_2 BCD_3 BCD_4 BCD_5 BCD_6
\   BCD_7 BCD_8 BCD_9 BCD_DSH BCD_E BCD_H BCD_L BCD_P BCD_BLK
\   BCD_PER_MSK SEG_G_BT SEG_F_BT SEG_E_BT SEG_D_BT SEG_C_BT SEG_B_BT
\   SEG_A_BT SEG_DP_BT
\
\ **********************************************************************

max7219defs
marker max7219defs

\ **********************************************************************
\ The pin number of the LOAD pin can be changed here.
\ **********************************************************************

DDRD constant LOAD_DDR
PORTD constant LOAD_PORTR
#4 constant LOAD_BT

\ **********************************************************************
\ Constants corresponding to Register Address Map in datasheet
\ **********************************************************************

$0000 constant NOOP_MSK
$0100 constant D0_MSK
$0200 constant D1_MSK
$0300 constant D2_MSK
$0400 constant D3_MSK
$0500 constant D4_MSK
$0600 constant D5_MSK
$0700 constant D6_MSK
$0800 constant D7_MSK
$0900 constant DEC_MODE_MSK
$0a00 constant INTEN_MSK
$0b00 constant SCAN_LIM_MSK
$0c00 constant SHUTDN_MSK
$0f00 constant DISP_TST_MSK

\ **********************************************************************
\ Code B Constants
\ **********************************************************************

#0 constant BCD_0
#1 constant BCD_1
#2 constant BCD_2
#3 constant BCD_3
#4 constant BCD_4
#5 constant BCD_5
#6 constant BCD_6
#7 constant BCD_7
#8 constant BCD_8
#9 constant BCD_9
#10 constant BCD_DSH
#11 constant BCD_E
#12 constant BCD_H
#13 constant BCD_L
#14 constant BCD_P
#15 constant BCD_BLK
%10000000 constant BCD_PER_MSK

\ **********************************************************************
\ Segment bit definitions
\
\ --A--
\ F   B
\ --G--
\ E   C
\ --D--  DP
\ **********************************************************************

#0 constant SEG_G_BT
#1 constant SEG_F_BT
#2 constant SEG_E_BT
#3 constant SEG_D_BT
#4 constant SEG_C_BT
#5 constant SEG_B_BT
#6 constant SEG_A_BT
#7 constant SEG_DP_BT
