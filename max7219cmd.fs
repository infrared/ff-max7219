\ **********************************************************************
\
\ max7219cmd.fs is part of the ff-max2719 project
\ ff-max2719 is a project to control the max2719 chip with FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides words for the controlling the max7219 chip over
\ SPI.
\
\ public words:
\   init-spi init-load load-16 test-mode shutdown-mode scan-limit
\   intensity decode-mode
\
\ **********************************************************************

max7219cmd
marker max7219cmd

\ **********************************************************************
\ Words for handling communication with the chip.
\ **********************************************************************

: init-spi ( -- )
    1 DD_MOSI   lshift
    1 DD_SCK    lshift or
    1 DD_SS     lshift or DDR_SPI mset
    1           SPR0 lshift
    0           SPR1 lshift or
    CPHA_SMPLED CPHA lshift or
    CPOL_LIDLE  CPOL lshift or
    MSTR_MSTR   MSTR lshift or
    DORD_MSB    DORD lshift or
    SPE_ENAB    SPE  lshift or
    SPIE_DISAB  SPIE lshift or SPCR c!
;

: init-load ( -- )  [ 1 LOAD_BT lshift ] literal LOAD_DDR mset ;

: load-low ( -- ) [ 1 LOAD_BT lshift ] literal LOAD_PORTR mclr ;

: load-high ( -- ) [ 1 LOAD_BT lshift ] literal LOAD_PORTR mset ;

: load-16 ( u -- ) load-low 2tx-spi load-high ;

\ **********************************************************************
\ Special modes test and shutdown. Flag true puts us into test mode
\ or into shutdown mode, respectively.
\ **********************************************************************

: test-mode ( flag -- ) if 1 else 0 then DISP_TST_MSK or load-16 ;

: shutdown-mode ( flag -- ) if 0 else 1 then SHUTDN_MSK or load-16 ;

\ **********************************************************************
\ scan-limit: to display N digits, pass in parameter N-1
\ intensity: pass in parameter between 0 (min) to #15 (max)
\ **********************************************************************

: scan-limit ( c -- ) SCAN_LIM_MSK or load-16 ;

: intensity ( c -- ) INTEN_MSK or load-16 ;

\ **********************************************************************
\ decode-mode: pass in a mask of digits you want to be in decode mode.
\ Pass in 0 for no digits decoded or $ff for all digits decoded.
\ **********************************************************************

: decode-mode ( c -- ) DEC_MODE_MSK or load-16 ;

