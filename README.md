# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/ff-max7219)](https://api.reuse.software/info/codeberg.org/infrared/ff-max7219)

Readme.md is part of the ff-max7219 project.

ff-max7219 is a project to control the max7219 chip with FlashForth.

Copyright (C) 2021  Christopher Howard

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Description

This project contains FlashForth code for a project to control a
max7219-based 7-segment LED module using an ATmega328P
microcontroller.

# Requirements

ATmega328P microcontroller connected to a max7219 chip. The Arduino
UNO was used for development.

This project was coded for controlling a max7219 chip, since I had
access to a max7219-based LED module, but I don't know of a reason why
it wouldn't work on the max7221.

The code was developed for this FlashForth build:

FlashForth 5 ATmega328 18.11.2020

The code assumes the LOAD pin (possibly labeled CS) is connected to
the pin PD4, which is pin DP4 on an UNO. But this can be changed by
adjusting constants in the `max7219defs.fs` file.

You will need the `doloop.fs` file, available in the `avr/forth`
directory of the FlashForth source code. It is not included in the
ff-ad9833 source since it is part of the FlashForth system.

# Compiling

After setting up a FlashForth Shell connection, run these commands in
the Shell.

In FlashForth, you only need to compile (#send) the code once, and
then you can run the compiled words later, even after resetting the
microcontroller. For more information about FlashForth, visit

[FlashForth Web Site](https://flashforth.com/)

## Core words and definitions for controlling the max7219

    #send doloop
    #send util
    #send 328p
    #send max7219defs
    #send max7219cmd

## Demo words

    #send max7219cmd-demo

# Use

This code can be used to display information on the max7219 chip,
using either Binary Coded Decimal, or by controlling the segments
individually. See the demo words `bcd-demo` and `nd-demo` in the
`max7219cmd-demo.fs` file for examples.

# Running Demos

After setting up a FlashForth shell connection, enter the `words`
command and look for words to execute which are prefixed with `demo-`.

# Code documentation

Each Forth code file has the following documentation in comments:
1. The title of the file
2. A copyright and license header
3. A description of the purpose of the file
4. A list of public words, i.e., words meant to be used outside of the
module itself
5. The code is divided into sections, each containing a section
description.
6. Each dictionary word, not including words like constants and
variables, contains a brief Forth-style stack effect description.

# Contact

Christopher Howard

e-mail address in Base64 encoding:

    Y2hyaXN0b3BoZXIgLUFULSBsaWJyZWhhY2tlciAtRE9ULSBjb20K