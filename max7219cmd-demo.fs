\ **********************************************************************
\
\ max7219cmd-demo.fs is part of the ff-max2719 project
\ ff-max2719 is a project to control the max2719 chip with FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides demo works for the ff-max2719 project.
\
\ public words:
\   blink-demo bcd-demo nd-demo
\
\ **********************************************************************

max7219cmd-demo
marker max7219cmd-demo

\ **********************************************************************
\ Blink the display by toggling test-mode and shutdown-mode
\ **********************************************************************

: blink ( -- ) true test-mode 500 ms false test-mode 500 ms ;

: blink-demo ( -- ) init-spi init-load true shutdown-mode 8 0 do blink loop ;

\ **********************************************************************
\ Decode Mode Demo. Displays all the characters of the chip's B font.
\ **********************************************************************

: bcd-demo ( -- )
    init-spi init-load $ff decode-mode
    8 0 do
    BCD_PER_MSK BCD_0 or   D0_MSK or load-16
                BCD_1      D1_MSK or load-16
    BCD_PER_MSK BCD_2 or   D2_MSK or load-16
                BCD_3      D3_MSK or load-16
    BCD_PER_MSK BCD_4 or   D4_MSK or load-16
                BCD_5      D5_MSK or load-16
    BCD_PER_MSK BCD_6 or   D6_MSK or load-16
                BCD_7      D7_MSK or load-16
    #1000 ms
                BCD_8      D0_MSK or load-16
    BCD_PER_MSK BCD_9 or   D1_MSK or load-16
                BCD_DSH    D2_MSK or load-16
    BCD_PER_MSK BCD_E or   D3_MSK or load-16
                BCD_H      D4_MSK or load-16
    BCD_PER_MSK BCD_L or   D5_MSK or load-16
                BCD_P      D6_MSK or load-16
    BCD_PER_MSK BCD_BLK or D7_MSK or load-16
    #1000 ms
    loop
;

\ **********************************************************************
\ No-Decode Mode Demo. Each segment on each digit lights up in a
\ sequence. Digits may display previous data or garbage data until
\ overwritten by the sequence.
\ **********************************************************************

#250 constant NDDEMO_DELAY

#64 constant NDDEMO_DATLEN

flash

create nd-demo-dat
D0_MSK 1 SEG_A_BT  lshift or ,
D0_MSK 1 SEG_B_BT  lshift or ,
D0_MSK 1 SEG_F_BT  lshift or ,
D0_MSK 1 SEG_G_BT  lshift or ,
D0_MSK 1 SEG_E_BT  lshift or ,
D0_MSK 1 SEG_C_BT  lshift or ,
D0_MSK 1 SEG_D_BT  lshift or ,
D0_MSK 1 SEG_DP_BT lshift or ,
D1_MSK 1 SEG_A_BT  lshift or ,
D1_MSK 1 SEG_B_BT  lshift or ,
D1_MSK 1 SEG_F_BT  lshift or ,
D1_MSK 1 SEG_G_BT  lshift or ,
D1_MSK 1 SEG_E_BT  lshift or ,
D1_MSK 1 SEG_C_BT  lshift or ,
D1_MSK 1 SEG_D_BT  lshift or ,
D1_MSK 1 SEG_DP_BT lshift or ,
D2_MSK 1 SEG_A_BT  lshift or ,
D2_MSK 1 SEG_B_BT  lshift or ,
D2_MSK 1 SEG_F_BT  lshift or ,
D2_MSK 1 SEG_G_BT  lshift or ,
D2_MSK 1 SEG_E_BT  lshift or ,
D2_MSK 1 SEG_C_BT  lshift or ,
D2_MSK 1 SEG_D_BT  lshift or ,
D2_MSK 1 SEG_DP_BT lshift or ,
D3_MSK 1 SEG_A_BT  lshift or ,
D3_MSK 1 SEG_B_BT  lshift or ,
D3_MSK 1 SEG_F_BT  lshift or ,
D3_MSK 1 SEG_G_BT  lshift or ,
D3_MSK 1 SEG_E_BT  lshift or ,
D3_MSK 1 SEG_C_BT  lshift or ,
D3_MSK 1 SEG_D_BT  lshift or ,
D3_MSK 1 SEG_DP_BT lshift or ,
D4_MSK 1 SEG_A_BT  lshift or ,
D4_MSK 1 SEG_B_BT  lshift or ,
D4_MSK 1 SEG_F_BT  lshift or ,
D4_MSK 1 SEG_G_BT  lshift or ,
D4_MSK 1 SEG_E_BT  lshift or ,
D4_MSK 1 SEG_C_BT  lshift or ,
D4_MSK 1 SEG_D_BT  lshift or ,
D4_MSK 1 SEG_DP_BT lshift or ,
D5_MSK 1 SEG_A_BT  lshift or ,
D5_MSK 1 SEG_B_BT  lshift or ,
D5_MSK 1 SEG_F_BT  lshift or ,
D5_MSK 1 SEG_G_BT  lshift or ,
D5_MSK 1 SEG_E_BT  lshift or ,
D5_MSK 1 SEG_C_BT  lshift or ,
D5_MSK 1 SEG_D_BT  lshift or ,
D5_MSK 1 SEG_DP_BT lshift or ,
D6_MSK 1 SEG_A_BT  lshift or ,
D6_MSK 1 SEG_B_BT  lshift or ,
D6_MSK 1 SEG_F_BT  lshift or ,
D6_MSK 1 SEG_G_BT  lshift or ,
D6_MSK 1 SEG_E_BT  lshift or ,
D6_MSK 1 SEG_C_BT  lshift or ,
D6_MSK 1 SEG_D_BT  lshift or ,
D6_MSK 1 SEG_DP_BT lshift or ,
D7_MSK 1 SEG_A_BT  lshift or ,
D7_MSK 1 SEG_B_BT  lshift or ,
D7_MSK 1 SEG_F_BT  lshift or ,
D7_MSK 1 SEG_G_BT  lshift or ,
D7_MSK 1 SEG_E_BT  lshift or ,
D7_MSK 1 SEG_C_BT  lshift or ,
D7_MSK 1 SEG_D_BT  lshift or ,
D7_MSK 1 SEG_DP_BT lshift or ,

ram

: nd-demo ( -- )
    init-spi init-load $00 decode-mode false test-mode #7 scan-limit
    false shutdown-mode
    NDDEMO_DATLEN 0 do
	i cells nd-demo-dat + @ load-16
	NDDEMO_DELAY ms
    loop
;
